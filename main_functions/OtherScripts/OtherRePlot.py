# -*- coding: utf-8 -*-
# https://blog.csdn.net/weixin_52040868
# 公众号：测个der
# 微信：qing_an_an


import re
import matplotlib.pyplot as plt
import csv

class PLot:
    def readLog(self, path):
        with open(path, 'r', encoding='utf-8') as r:
            values = r.read()
        return values

    def CreatePlot(self, path, args1, args2, title):
        """
        :param path: 文件路径
        :param args1: 正则表达式1
        :param args2: 正则表达式2
        """
        APRPM_dict = {}

        values = self.readLog(path)
        APRPM = re.findall(args1, values)   # 匹配APRPM
        BPRPM = re.findall(args2, values)   # 匹配BPRPM

        A_LEN = [i for i in range(len(APRPM))]  # 生成行数

        F_APRPM = [float(j) for j in APRPM] # 转化浮点数
        F_BPRPM = [float(j) for j in BPRPM] # 转化浮点数

        plt.plot(A_LEN, F_APRPM,label='APRPM')    # 确定XY轴
        plt.plot(A_LEN, F_BPRPM,label='BPRPM')    # 确定XY轴
        plt.title(title)
        plt.legend()
        plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
        # plt.grid(True)              # 展现网格线
        plt.xlabel("功率起始20W/每隔5个数据功率+10w")
        plt.ylabel("转速")
        plt.show()                  # 展现折线图


if __name__ == '__main__':
    plot = PLot()
    plot.CreatePlot(r'C:\Users\admin\Desktop\2023_06_01_15_38_48.log',
                    args1='APRPM\s*:\s*(\d+[.,]?\d*)',
                    args2='BPRPM\s*:\s*(.*\d*)',
                    title="左右喷水电机折线图")
