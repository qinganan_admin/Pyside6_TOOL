# -*- coding: utf-8 -*-
# https://blog.csdn.net/weixin_52040868
# 公众号：测个der
# 微信：qing_an_an


"""单项数据筛选测试"""
import re
import os
import pandas as pd
import datetime

def autoReadLog(func):
    def wrapper(*args, **kwargs):
        self = args[0]
        with open(r"C:\Users\admin\Desktop\2023_08_31_17_07_19.log", 'r', encoding='utf-8') as r:
            self.texts = r.read()
        return func(*args, **kwargs)

    return wrapper


class LogParsing:
    def __init__(self):
        self.texts = ' '
        self.dataDict = {}
        self.path = r"C:\Users\admin\Desktop"

    @autoReadLog
    def reInfoParsing(self):
        """
        :return: info数据筛选
        """
        values = re.findall('<.*?>', self.texts)
        orderValues = list(dict.fromkeys(values))
        for order, num in zip(orderValues, range(len(orderValues) - 1)):
            pattern = r"{}([\s\S]*?){}".format(re.escape(values[num]), re.escape(values[num + 1]))
            datas = re.findall(pattern, self.texts)
            for data in datas:
                # dataValue = re.findall('(.*])(\w*\s*):\s*(.*)', data)
                dataValue = re.findall('\[(.*)\]\s*(.*)\s*:\s*([^\s]*)', data)
                if order not in self.dataDict:
                    self.dataDict[order] = {}
                    self.dataDict[order]['time'] = []

                for dataDict in dataValue:
                    time, key, value = dataDict[0], dataDict[1].strip(), dataDict[2]
                    if key not in self.dataDict[order]:
                        self.dataDict[order][key] = []
                    self.dataDict[order][key].append(value)

                self.dataDict[order]['time'].append(time)

        self.writerInfoExcel(self.path + "\\日志解析数据.xlsx")

    def writerInfoExcel(self, filePath: str):
        """`
        :param filePath: # 存储路径
        :return:   做数据图
        """
        with pd.ExcelWriter(filePath) as writer:
            for sheet_name, data in self.dataDict.items():
                print(data)
                df = pd.DataFrame(data)
                for column in df.columns:
                    if df[column].dtype == object:  # 检查列的数据类型是否为对象类型
                        try:
                            # 尝试将值转换为浮点数
                            df[column] = df[column].astype(float)
                        except ValueError:
                            # 转换失败，将值保持为字符串
                            df[column] = df[column].astype(str)
                df.to_excel(writer, sheet_name=sheet_name, index=False)

if __name__ == '__main__':
    LogParsing().reInfoParsing()